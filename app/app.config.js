(function(){
    'use strict';
    angular
        .module('policingApp')
        .run(sec)
        .config(routes)
        .config(routeFixer)
        .constant('_', window._);


    function sec($rootScope,$location){
      $rootScope.$on("$routeChangeError", function(event, next, previous, error) {
        // We can catch the error thrown when the $requireSignIn promise is rejected
        // and redirect the user back to the home page
        if (error === "AUTH_REQUIRED") {
          $location.path("/login");
        }
      });
    }    


    function routes($urlRouterProvider){
    // if the path doesn't match any of the urls configured
    // otherwise will take care of routing the user to the home page
    $urlRouterProvider.otherwise('/');

    }

    //To fix the angular 1.6 url routes changes
  function routeFixer($locationProvider) {
    $locationProvider.hashPrefix('');
  }

}());
