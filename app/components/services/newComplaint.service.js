(function(){
    'use strict';

    angular
        .module('policingApp')
        .factory('NewComplaintService', NewComplaintService);

        var complaintId;
        function NewComplaintService(FirebaseService){
            var service ={
                saveComplaintForm:saveComplaintForm,
                getComplaintId : getComplaintId,
                setComplaintId : setComplaintId
            }
            return service

         function saveComplaintForm(complaint){
            var databaseRef = FirebaseService.firebaseDatabase();
                  var newId = databaseRef.ref().child('Complaints').push().key;
                  var updates = {};
                  updates['/Complaints/' + newId] = complaint;
                  //Send the text messag
                  setComplaintId(newId);
                  return databaseRef.ref().update(updates);
          }
          function setComplaintId(id){
              complaintId = id;
          }
          function getComplaintId(){
              return complaintId;
          }
        }


}());