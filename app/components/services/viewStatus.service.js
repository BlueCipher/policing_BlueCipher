(function(){
    'use strict';

    angular
    .module('policingApp')
    .factory('ViewStatusService',ViewStatusService);


    function ViewStatusService(FirebaseService, $firebaseObject){
      var databaseRef = FirebaseService.firebaseDatabase().ref();
      var complaintId;
     

      var service = {
          getStatus:getStatus,
          setComplaintId:setComplaintId,
          getComplaintId:getComplaintId
      };
      return service;

      function getStatus(id) {
        
          return $firebaseObject(databaseRef.child('Complaints').child(id));
      }
      function getComplaintId(){
          return complaintId;
      }
      function setComplaintId(id){
        complaintId = id;
      }
    }

}());