(function(){
    'use strict';

    angular
    .module('policingApp')
    .factory('ViewFIRService',ViewFIRService);


    function ViewFIRService(FirebaseService){
      var databaseRef = FirebaseService.firebaseDatabase();
      var FIRs=[{}];
      var selectedFir;
     

      var service = {
        getTheFIRsList:getTheFIRsList,
        setSelectedFir:setSelectedFir,
        getSelectedFir:getSelectedFir,
        updateFIR:updateFIR
      };
      return service;

      function getTheFIRsList() {
  
          return databaseRef.ref().child('FIRs');
      }
      function updateFIR(details,id){
            var update = {
              aadhaar:details.aadhaar,
              address:details.address,
              description:details.description,
              doo:details.doo,
              dor:details.dor,
              firstName:details.firstName,
              gender:details.gender,
              lastName:details.lastName,
              mobile:details.mobile,
              place:details.place,
              policeStation:details.policeStation,
              state:details.state,
              status:details.status,
              subject:details.subject
              
            }    
        return databaseRef.ref('FIRs/'+id).set(update);
      }

      function setSelectedFir(fir){
        selectedFir = fir;
    }
      function getSelectedFir(){
        return selectedFir;
      }
    }

}());
