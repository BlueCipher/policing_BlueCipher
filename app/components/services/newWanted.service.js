(function(){
    'use strict';

    angular
        .module('policingApp')
        .factory('NewWantedService', NewWantedService);

        function NewWantedService(FirebaseService){
            var service ={
                saveWantedForm:saveWantedForm
            }
            return service

         function saveWantedForm(mostWanted){
            var databaseRef = FirebaseService.firebaseDatabase();
                  var newId = databaseRef.ref().child('wanted').push().key;
                  var updates = {};
                  updates['/wanted/' + newId] = mostWanted;
                  return databaseRef.ref().update(updates);
          }
        }


}());