(function(){
    'use strict';

    angular
    .module('policingApp')
    .factory('DetaineesListService',DetaineesListService);


    function DetaineesListService(FirebaseService){
      var databaseRef = FirebaseService.firebaseDatabase();
      var detainees=[{}];
      var selectedPerson;
     

      var service = {
        getTheDetaineesList:getTheDetaineesList,
        setSelectedPerson:setSelectedPerson,
        getSelectedPerson:getSelectedPerson
       
      };
      return service;

      function getTheDetaineesList() {
          return databaseRef.ref().child('detainees');
      }

      function setSelectedPerson(person){
        selectedPerson = person;
    }
      function getSelectedPerson(){
        return selectedPerson;
      }
    }

}());
