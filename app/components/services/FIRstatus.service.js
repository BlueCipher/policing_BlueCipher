(function(){
    'use strict';

    angular
    .module('policingApp')
    .factory('FIRstatusService',FIRstatusService);


    function FIRstatusService(FirebaseService, $firebaseObject){
      var databaseRef = FirebaseService.firebaseDatabase().ref();
      var FIRId;
     

      var service = {
          getStatus:getStatus,
          setFIRId:setFIRId,
          getFIRId:getFIRId
      };
      return service;

      function getStatus(id) {
        
          return $firebaseObject(databaseRef.child('FIRs').child(id));
      }
      function getFIRId(){
          return FIRId;
      }
      function setFIRId(id){
        FIRId = id;
      }
    }

}());