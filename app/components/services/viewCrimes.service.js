(function(){
    'use strict';

    angular
    .module('policingApp')
    .factory('ViewCrimesService',ViewCrimesService);


    function ViewCrimesService(FirebaseService){
      var databaseRef = FirebaseService.firebaseDatabase();
      var crimes=[{}];
      var selectedCrime;

      var service = {
        getTheCrimesList:getTheCrimesList,
        setSelectedCrime:setSelectedCrime,
        getSelectedCrime:getSelectedCrime,
        updateCrime:updateCrime,
       
      };
      return service;

      function getTheCrimesList() {
      
          return databaseRef.ref().child('crimes');
      }

      function setSelectedCrime(crime){
        selectedCrime = crime;
    }
      function getSelectedCrime(){
        return selectedCrime;
      }

      function updateCrime(details,id){
        var update = {
          aadhaar:details.aadhaar,
          address:details.address,
          date:details.date,
          description:details.description,
          dob:details.dob,
          firstName:details.firstName,
          gender:details.gender,
          lastName:details.lastName,
          mobile:details.mobile,
          place:details.place,
          state:details.state,
          status:details.status,
          subject:details.subject
          
        }    
    return databaseRef.ref('crimes/'+id).set(update);
  }
    }

}());
