(function(){
    'use strict';

    angular
        .module('policingApp')
        .factory('AuthService', authService);

        function authService(FirebaseService,$firebaseAuth){
            var service ={
               login:login,
                loginAnonymously:loginAnonymously,
                logout:logout,
                auth:auth
            }
            return service

          function login(email,password){
              var auth = FirebaseService.firebaseAuth();
             return auth.signInWithEmailAndPassword(email,password);
          }

          function logout(){
            var auth = FirebaseService.firebaseAuth();
           return auth.signOut();
        }


          function loginAnonymously() {
            var auth = FirebaseService.firebaseAuth();
            return auth.signInAnonymously()
          }

          function auth(){
              return $firebaseAuth();
          }
        }


}());
