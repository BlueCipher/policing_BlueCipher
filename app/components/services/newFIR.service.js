(function(){
    'use strict';

    angular
        .module('policingApp')
        .factory('NewFIRService', NewFIRService);

        var FIRId;
        
        function NewFIRService(FirebaseService){
            var service ={
                saveFIRForm:saveFIRForm,
                getFIRId : getFIRId,
                setFIRId : setFIRId,
         
            }
            return service

         function saveFIRForm(FIR){
            var databaseRef = FirebaseService.firebaseDatabase();
                  var newId = databaseRef.ref().child('FIRs').push().key;
                  var updates = {};
                  updates['/FIRs/' + newId] = FIR;
                  //Send the text messag
                  setFIRId(newId); 
                  return databaseRef.ref().update(updates);
          }
          function setFIRId(id){
              FIRId = id;
          }
          function getFIRId(){
              return FIRId;
          }

          
        }


}());