(function(){
    'use strict';

    angular
        .module('policingApp')
        .factory('NewDetaineeService', NewDetaineeService);

        function NewDetaineeService(FirebaseService){
            var service ={
                saveDetaineeForm:saveDetaineeForm
            }
            return service

         function saveDetaineeForm(newDetainee){
            var databaseRef = FirebaseService.firebaseDatabase();
                  var newId = databaseRef.ref().child('detainees').push().key;
                  var updates = {};
                  updates['/detainees/' + newId] = newDetainee;
                  return databaseRef.ref().update(updates);
          }
        }


}());