(function(){
    'use strict';

    angular
    .module('policingApp')
    .factory('WantedListService',WantedListService);


    function WantedListService(FirebaseService){
      var databaseRef = FirebaseService.firebaseDatabase();
      var wanted=[{}];
      var selectedPerson;
     

      var service = {
        getTheWantedList:getTheWantedList,
        setSelectedPerson:setSelectedPerson,
        getSelectedPerson:getSelectedPerson
       
      };
      return service;

      function getTheWantedList() {
          return databaseRef.ref().child('wanted');
      }

      function setSelectedPerson(person){
        selectedPerson = person;
    }
      function getSelectedPerson(){
        return selectedPerson;
      }
    }

}());
