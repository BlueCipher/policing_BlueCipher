(function(){
    'use strict';

    angular
    .module('policingApp')
    .factory('ViewComplaintsService',ViewComplaintsService);


    function ViewComplaintsService(FirebaseService){
      var databaseRef = FirebaseService.firebaseDatabase();
      var complaints=[{}];
      var selectedComplaint
     

      var service = {
        getTheComplaintsList:getTheComplaintsList,
        setSelectedComplaint:setSelectedComplaint,
        getSelectedComplaint:getSelectedComplaint,
        updateComplaint:updateComplaint,
      };
      return service;

      function getTheComplaintsList() {
        
          return databaseRef.ref().child('Complaints');
      }

      function updateComplaint(details,id){
        var update = {
          aadhaar:details.aadhaar,
          address:details.address,
          date:details.date,
          description:details.description,
          dob:details.dob,
          firstName:details.firstName,
          gender:details.gender,
          lastName:details.lastName,
          mobile:details.mobile,
          place:details.place,
          state:details.state,
          status:details.status,
          subject:details.subject
          
        }    
    return databaseRef.ref('Complaints/'+id).set(update);
  }

      function setSelectedComplaint(complaint){
        selectedComplaint = complaint;
     }
      function getSelectedComplaint(){
        return selectedComplaint;
      }
    }

}());
