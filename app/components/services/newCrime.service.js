(function(){
    'use strict';

    angular
        .module('policingApp')
        .factory('NewCrimeService', NewCrimeService);

        function NewCrimeService(FirebaseService){
            var service ={
                saveCrimeForm:saveCrimeForm
            }
            return service

         function saveCrimeForm(crime){
            var databaseRef = FirebaseService.firebaseDatabase();
                  var newId = databaseRef.ref().child('crimes').push().key;
                  var updates = {};
                  updates['/crimes/' + newId] = crime;
                  return databaseRef.ref().update(updates);
          }
     
 
        }


}());