(function(){
    'use strict';

/**
 * @ngdoc overview
 * @name policingApp
 * @description
 * # policingApp
 *
 * Main module of the application.
 */

    angular
        .module('policingApp',
				[
                'ngMessages',
                'ui.router',
                'policingApp.states.home',
                'policingApp.states.newComplaint',
                'policingApp.states.newCrime',
                'policingApp.states.wanted',
                'policingApp.states.officials',
                'policingApp.states.login',
                'policingApp.states.wanted.details',
                'policingApp.states.newComplaint.showId',
                'policingApp.states.complaintStatus',
                'policingApp.states.viewStatus',
                'policingApp.states.detainees',
                'policingApp.states.detainees.detaineeDetails',
                'policingApp.states.crimeState',
                'policingApp.states.court',
                'policingApp.states.newFIR',
                'policingApp.states.FIRId',
                'policingApp.states.FIRstatus',
                'policingApp.states.viewFIRstatus',
                'policingApp.states.adminPanel',
                'policingApp.states.adminFir',
                'policingApp.states.adminCrime',
                'policingApp.states.adminComplaint',
                'mgcrea.ngStrap',
                'ngSanitize',
                'hm.readmore',
                'ngAnimate',
                 'firebase',
                  'ngStorage'
        ]);

}());
