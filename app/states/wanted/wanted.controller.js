/**
 * Created by Patrick on 5/Jan/2018.
 */


(function () {
    'use strict';
  
    angular
      .module('policingApp.states.wanted')
      .controller('WantedController', WantedController);
  
    /* @ngInject */
   function WantedController($state,WantedListService,$firebaseArray){
    var vm = this;
    vm.allWanted=[];
    vm.getWanted = getAllWanted;
    vm.goToDetails = goToDetails;
   
    vm.getWanted();

    function getAllWanted(){
      vm.allWanted= $firebaseArray(WantedListService.getTheWantedList());
  
    }

    function goToDetails(person){
      WantedListService.setSelectedPerson(person);
      $state.go('details');
    }
   }
  }
  ());