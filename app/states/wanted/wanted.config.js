/**
 * Created by Batch 3 on 5/Jan/2018.
 */

(function () {
    'use strict';
  
    angular
      .module('policingApp.states.wanted',[])
      .config(routes);
  
    function routes($stateProvider){
      $stateProvider
  
        .state('wanted', {
          url: '/wanted',
          controller:'WantedController',
          controllerAs:'wanted',
          templateUrl: 'states/wanted/wanted.html'
  
        }) ;
    }
  }());