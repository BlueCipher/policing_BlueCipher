(function () {
    'use strict';
  
    angular
      .module('policingApp.states.adminFir')
      .controller('AdminFirController', AdminFirController);
  
    /* @ngInject */
   function AdminFirController($state, ViewFIRService,NewFIRService,currentUser,AuthService){
     var vm = this;

     vm.currentUser = currentUser;
     if(vm.currentUser == null){
       $state.go("login");
     }

     vm.forwardFIR=forwardFIR;
     vm.firDetails = ViewFIRService.getSelectedFir();

     if(!vm.firDetails) {
      $state.go('adminPanel');
     }
     function forwardFIR(){
       vm.firDetails.status = 'Forwarded';
       ViewFIRService.updateFIR(vm.firDetails,vm.firDetails.$id);
     }
   }
  }
  ());