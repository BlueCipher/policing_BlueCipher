(function (){
    'use strict';

    angular
        .module('policingApp.states.adminFir',[])
        .config(routes);

    function routes($stateProvider){
        $stateProvider
            .state('adminFir',{
                url:'/adminFir',
                controller:'AdminFirController',
                controllerAs:'adminFir',
                templateUrl:'states/adminFir/adminFir.html',
                resolve: {
                    // controller will not be loaded until $waitForAuth resolves
                    // Auth refers to our $firebaseAuth wrapper in the example above
                    currentUser:  function(AuthService) {
                      // $waitForAuth returns a promise so the resolve waits for it to complete
                      return AuthService.auth().$waitForSignIn();
                    }
                  }
            }
        )
            
    }
}
()
);