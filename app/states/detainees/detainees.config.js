/**
 * Created by Batch 3 on 2/Feb/2018.
 */

(function () {
    'use strict';
  
    angular
      .module('policingApp.states.detainees',[])
      .config(routes);
  
    function routes($stateProvider){
      $stateProvider
  
        .state('detainees', {
          url: '/detainees',
          controller:'DetaineesController',
          controllerAs:'detainees',
          templateUrl: 'states/detainees/detainees.html'
  
        }) ;
    }
  }());