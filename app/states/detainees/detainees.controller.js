/**
 * Created by Patrick on 2/Feb/2018.
 */


(function () {
    'use strict';
  
    angular
      .module('policingApp.states.detainees')
      .controller('DetaineesController', DetaineesController);
  
    /* @ngInject */
   function DetaineesController($state,DetaineesListService,$firebaseArray){
    var vm = this;
    vm.allDetainees=[];
    vm.getDetainees = getAllDetainees;
    vm.goToDetails = goToDetails;
   
    vm.getDetainees();

    function getAllDetainees(){
      vm.allDetainees= $firebaseArray(DetaineesListService.getTheDetaineesList());
    }

    function goToDetails(person){
      DetaineesListService.setSelectedPerson(person);
      $state.go('detaineeDetails');
    }
   }
  }
  ());