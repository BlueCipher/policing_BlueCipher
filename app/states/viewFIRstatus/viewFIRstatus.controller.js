(function () {
    'use strict';
  
    angular
      .module('policingApp.states.viewFIRstatus')
      .controller('ViewFIRstatusController', ViewFIRstatusController);
  
    /* @ngInject */
   function ViewFIRstatusController($state, FIRstatusService){
     var vm = this;
     vm.id=FIRstatusService.getFIRId();
     vm.FIR=FIRstatusService.getStatus(vm.id);

   }
  }
  ());