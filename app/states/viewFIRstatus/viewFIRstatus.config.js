(function (){
    'use strict';

    angular
        .module('policingApp.states.viewFIRstatus',[])
        .config(routes);

    function routes($stateProvider){
        $stateProvider
            .state('viewFIRstatus',{
                url:'/viewFIRstatus',
                controller:'ViewFIRstatusController',
                controllerAs:'viewFIRstatus',
                templateUrl:'states/viewFIRstatus/viewFIRstatus.html'
            }
        )
            
    }
}
()
);