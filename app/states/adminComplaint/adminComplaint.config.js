(function (){
    'use strict';

    angular
        .module('policingApp.states.adminComplaint',[])
        .config(routes);

    function routes($stateProvider){
        $stateProvider
            .state('adminComplaint',{
                url:'/adminComplaint',
                controller:'AdminComplaintController',
                controllerAs:'adminComplaint',
                templateUrl:'states/adminComplaint/adminComplaint.html',
                resolve: {
                    // controller will not be loaded until $waitForAuth resolves
                    // Auth refers to our $firebaseAuth wrapper in the example above
                    currentUser:  function(AuthService) {
                      // $waitForAuth returns a promise so the resolve waits for it to complete
                      return AuthService.auth().$waitForSignIn();
                    }
                  }
            }
        )
            
    }
}
()
);