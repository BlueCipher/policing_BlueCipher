(function () {
    'use strict';
  
    angular
      .module('policingApp.states.adminComplaint')
      .controller('AdminComplaintController', AdminComplaintController);
  
    /* @ngInject */
   function AdminComplaintController($state, ViewComplaintsService,currentUser,AuthService){
     var vm = this;

     vm.currentUser = currentUser;
     if(vm.currentUser == null){
       $state.go("login");
     }

     vm.forwardComplaint=forwardComplaint;
     vm.complaintDetails = ViewComplaintsService.getSelectedComplaint();

     if(!vm.complaintDetails) {
      $state.go('adminPanel');
     }

     function forwardComplaint(){
      vm.complaintDetails.status = 'Forwarded';
      ViewComplaintsService.updateComplaint(vm.complaintDetails,vm.complaintDetails.$id);
    }
   }
  }
  ());