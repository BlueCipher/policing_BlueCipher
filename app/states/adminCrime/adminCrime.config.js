(function (){
    'use strict';

    angular
        .module('policingApp.states.adminCrime',[])
        .config(routes);

    function routes($stateProvider){
        $stateProvider
            .state('adminCrime',{
                url:'/adminCrime',
                controller:'AdminCrimeController',
                controllerAs:'adminCrime',
                templateUrl:'states/adminCrime/adminCrime.html',
                resolve: {
                    // controller will not be loaded until $waitForAuth resolves
                    // Auth refers to our $firebaseAuth wrapper in the example above
                    currentUser:  function(AuthService) {
                      // $waitForAuth returns a promise so the resolve waits for it to complete
                      return AuthService.auth().$waitForSignIn();
                    }
                  }
            }
        )
            
    }
}
()
);