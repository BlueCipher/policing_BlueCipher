(function () {
    'use strict';
  
    angular
      .module('policingApp.states.adminCrime')
      .controller('AdminCrimeController', AdminCrimeController);
  
    /* @ngInject */
   function AdminCrimeController($state, ViewCrimesService,currentUser,AuthService){
     var vm = this;
     vm.currentUser = currentUser;
     if(vm.currentUser == null){
       $state.go("login");
     }

     vm.forwardCrime=forwardCrime;
     vm.CrimeDetails = ViewCrimesService.getSelectedCrime();

     if(!vm.CrimeDetails) {
      $state.go('adminPanel');
     }

     function forwardCrime(){
      vm.CrimeDetails.status = 'Forwarded';
      ViewCrimesService.updateCrime(vm.CrimeDetails,vm.CrimeDetails.$id);
    }
   }
  }
  ());