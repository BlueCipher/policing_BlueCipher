/**
 * Created by Patrick on 4/Jan/2018.
 */

(function () {
    'use strict';
  
    angular
      .module('policingApp.states.newComplaint',[])
      .config(routes);
  
    function routes($stateProvider){
      $stateProvider
  
        .state('newComplaint', {
          url: '/newComplaint',
          controller:'NewComplaintController',
          controllerAs:'newComplaint',
          templateUrl: 'states/newComplaint/newComplaint.html'
  
        }) ;
    }
  }());