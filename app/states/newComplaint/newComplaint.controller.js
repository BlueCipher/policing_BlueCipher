/**
 * Created by Batch 3 on 4/Jan/2018.
 */


(function () {
    'use strict';
  
    angular
      .module('policingApp.states.newComplaint')
      .controller('NewComplaintController', NewComplaintController);
  
    /* @ngInject */
   function NewComplaintController($state, NewComplaintService){
     var vm = this;
     vm.submitForm = submitForm;
     vm.name = "Form";

     function submitForm(){
       vm.complaintForm.status= 'recieved';
       NewComplaintService.saveComplaintForm(vm.complaintForm).then(function (success) {
         
        $state.go('showId');
     }).catch(function (error) {
     window.alert('Error',error);
   });

     }
   }
  }
  ());