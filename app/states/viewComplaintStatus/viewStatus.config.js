(function (){
    'use strict';

    angular
        .module('policingApp.states.viewStatus',[])
        .config(routes);

    function routes($stateProvider){
        $stateProvider
            .state('viewStatus',{
                url:'/viewStatus',
                controller:'ViewStatusController',
                controllerAs:'viewStatus',
                templateUrl:'states/viewComplaintStatus/viewStatus.html'
            }
        )
            
    }
}
()
);