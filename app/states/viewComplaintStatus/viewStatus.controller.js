(function () {
    'use strict';
  
    angular
      .module('policingApp.states.viewStatus')
      .controller('ViewStatusController', ViewStatusController);
  
    /* @ngInject */
   function ViewStatusController($state, ViewStatusService){
     var vm = this;
     vm.id=ViewStatusService.getComplaintId();
    
     vm.complaint=ViewStatusService.getStatus(vm.id);  
   }
  }
  ());