(function(){

    'use strict';

    angular
        .module('policingApp.states.crimeState',[])
        .config(routes);

    function routes($stateProvider){
        $stateProvider
            .state('crimeState',{
                url : '/crimeState',
                controller : 'CrimeStateController',
                controllerAs : 'crimeState',
                templateUrl : 'states/crimeState/crimeState.html'
            })

    }

}());