(function () {
    'use strict';
  
    angular
      .module('policingApp.states.newComplaint.showId')
      .controller('ShowIdController', ShowIdController);
  
    /* @ngInject */
   function ShowIdController($state, NewComplaintService){
     var vm = this;
     vm.showComplaintId = NewComplaintService.getComplaintId();     
   }
  }
  ());