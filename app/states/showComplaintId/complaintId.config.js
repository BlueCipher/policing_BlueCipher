(function (){
    'use strict';

    angular
        .module('policingApp.states.newComplaint.showId',[])
        .config(routes);

    function routes($stateProvider){
        $stateProvider
            .state('showId',{
                url:'/complaintId',
                controller:'ShowIdController',
                controllerAs:'showId',
                templateUrl:'states/showComplaintId/complaintId.html'
            }
        )
            
    }
}
()
);