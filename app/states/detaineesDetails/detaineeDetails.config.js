(function (){
    'use strict';

    angular
        .module('policingApp.states.detainees.detaineeDetails',[])
        .config(routes);

    function routes($stateProvider){
        $stateProvider
            .state('detaineeDetails',{
                url:'/detaineeDetails',
                controller:'DetaineeDetailsController',
                controllerAs:'detaineeDetails',
                templateUrl:'states/detaineesDetails/detaineeDetails.html'
            }
        )
            
    }
}
()
);