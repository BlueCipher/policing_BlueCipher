(function () {
    'use strict';
  
    angular
      .module('policingApp.states.detainees.detaineeDetails')
      .controller('DetaineeDetailsController', DetaineeDetailsController);
  
    /* @ngInject */
   function DetaineeDetailsController($state, DetaineesListService){
     var vm = this;

     vm.personDetails = DetaineesListService.getSelectedPerson();
     if(!vm.personDetails) {
      $state.go('detainees');
     }
   }
  }
  ());