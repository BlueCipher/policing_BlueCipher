/**
 * Created by Batch3 on 4/Jan/2018.
 */


(function () {
    'use strict';
  
    angular
      .module('policingApp.states.newCrime')
      .controller('NewCrimeController', NewCrimeController);
  
    /* @ngInject */
   function NewCrimeController($state,NewCrimeService){
     var vm = this;
     vm.submitForm = submitForm;
     vm.name = "Form";

     function submitForm(){
       vm.crimeForm.status= 'recieved';
       NewCrimeService.saveCrimeForm(vm.crimeForm).then(function (success) {
         
        $state.go('crimeState');
     }).catch(function (error) {
     window.alert('Error',error);
   });

     }

   }
  }
  ());