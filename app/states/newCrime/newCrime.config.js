/**
 * Created by Batch3 on 4/Jan/2018.
 */

(function () {
    'use strict';
  
    angular
      .module('policingApp.states.newCrime',[])
      .config(routes);
  
    function routes($stateProvider){
      $stateProvider
  
        .state('newCrime', {
          url: '/newCrime',
          controller:'NewCrimeController',
          controllerAs:'newCrime',
          templateUrl: 'states/newCrime/newCrime.html'
  
        }) ;
    }
  }());