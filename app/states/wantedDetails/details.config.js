(function (){
    'use strict';

    angular
        .module('policingApp.states.wanted.details',[])
        .config(routes);

    function routes($stateProvider){
        $stateProvider
            .state('details',{
                url:'/WantedDetails',
                controller:'DetailsController',
                controllerAs:'details',
                templateUrl:'states/wantedDetails/details.html'
            }
        )
            
    }
}
()
);