(function () {
    'use strict';
  
    angular
      .module('policingApp.states.wanted.details')
      .controller('DetailsController', DetailsController);
  
    /* @ngInject */
   function DetailsController($state, WantedListService){
     var vm = this;
     vm.personDetails = WantedListService.getSelectedPerson();
     if(!vm.personDetails) {
      $state.go('wanted');
     }
   }
  }
  ());