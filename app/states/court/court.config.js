(function(){

    'use strict';

    angular
        .module('policingApp.states.court',[])
        .config(routes);

    function routes($stateProvider){
        $stateProvider
            .state('court',{
                url : '/court',
                controller : 'CourtController',
                controllerAs : 'court',
                templateUrl : 'states/court/court.html'
            })

    }

}());