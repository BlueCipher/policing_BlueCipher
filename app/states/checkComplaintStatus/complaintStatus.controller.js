(function () {
    'use strict';
  
    angular
      .module('policingApp.states.complaintStatus')
      .controller('ComplaintStatusController', ComplaintStatusController);
  
    /* @ngInject */
   function ComplaintStatusController($state, $firebaseArray, ViewComplaintsService,ViewStatusService){
     var vm = this;
     vm.allComplaints=[];
     vm.record;
     vm.viewDetails=viewDetails;
     vm.getComplaints = getAllComplaints;
  
     vm.getComplaints();

  function getAllComplaints(){
    vm.allComplaints= $firebaseArray(ViewComplaintsService.getTheComplaintsList());
 
  }
  function getRecord(id){
    vm.record=allComplaints.$getRecord(id);
  }

  function viewDetails(){
    ViewStatusService.setComplaintId(vm.complaintSearch.complaintId);
    $state.go('viewStatus');
  }
   }
  }
  ());