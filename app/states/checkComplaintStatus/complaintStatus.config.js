(function (){
    'use strict';

    angular
        .module('policingApp.states.complaintStatus',[])
        .config(routes);

    function routes($stateProvider){
        $stateProvider
            .state('complaintStatus',{
                url:'/complaintStatus',
                controller:'ComplaintStatusController',
                controllerAs:'complaintStatus',
                templateUrl:'states/checkComplaintStatus/complaintStatus.html'
            }
        )
            
    }
}
()
);