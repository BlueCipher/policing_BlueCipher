(function () {
    'use strict';
  
    angular
      .module('policingApp.states.adminPanel')
      .controller('AdminPanelController', AdminPanelController);
  
    /* @ngInject */
   function AdminPanelController($state,ViewComplaintsService,ViewFIRService,ViewCrimesService,NewWantedService,NewDetaineeService,AuthService,$firebaseArray,currentUser){
    var vm = this;
    vm.currentUser = currentUser;
    if(vm.currentUser == null){
      $state.go("login");
    }
    

    vm.close=close;

    function close(){
      AuthService.logout().then(function (success) {
         
        $state.go('login');
     }).catch(function (error) {
     window.alert("cannot log out at the moment");
   });
    }


    vm.allComplaints=[];
    vm.allCrimes=[];
    vm.allFIRs=[];
    vm.getComplaints = getAllComplaints;
    vm.getCrimes = getAllCrimes;
    vm.getFIRs = getAllFIRs;

    vm.goToFirDetails = goToFirDetails;
    vm.goToCrimeDetails = goToCrimeDetails;
    vm.goToComplaintDetails = goToComplaintDetails;

    vm.submitWantedForm = submitWantedForm;
    vm.submitDetaineeForm=submitDetaineeForm;
    vm.name="detaineeForm";
    vm.name ="wantedForm";
    
    vm.getComplaints();
    vm.getCrimes();
    vm.getFIRs();
  
    function getAllComplaints(){
      vm.allComplaints= $firebaseArray(ViewComplaintsService.getTheComplaintsList());
   
    }

    function getAllFIRs(){
      vm.allFIRs= $firebaseArray(ViewFIRService.getTheFIRsList());
   
    }
    function getAllCrimes(){
      vm.allCrimes= $firebaseArray(ViewCrimesService.getTheCrimesList());
   
    }

    function goToFirDetails(fir){
      ViewFIRService.setSelectedFir(fir);
      $state.go('adminFir');
    }

    function goToCrimeDetails(crime){
      ViewCrimesService.setSelectedCrime(crime);
      $state.go('adminCrime');
    }

    function goToComplaintDetails(complaint){
      ViewComplaintsService.setSelectedComplaint(complaint);
      $state.go('adminComplaint');
    }

    function submitWantedForm(){
      NewWantedService.saveWantedForm(vm.wantedForm).then(function (success) {
        $state.reload();
        window.alert("New wanted person added!")
    }).catch(function (error) {
    window.alert('Error',error);
  });
    }

    function submitDetaineeForm(){
      NewDetaineeService.saveDetaineeForm(vm.detaineeForm).then(function (success) {
        $state.reload();
        window.alert("New detainee added!")
    }).catch(function (error) {
    window.alert('Error',error);
  });
    }

   }
  }
  ());
  