(function () {
    'use strict';
  
    angular
      .module('policingApp.states.adminPanel',[])
      .config(routes);
  
    function routes($stateProvider){
      $stateProvider
        
        .state('adminPanel', {
          url: '/adminPanel',
          controller:'AdminPanelController',
          controllerAs:'adminPanel',
          templateUrl: 'states/adminPanel/adminPanel.html',
          resolve: {
            // controller will not be loaded until $waitForAuth resolves
            // Auth refers to our $firebaseAuth wrapper in the example above
            currentUser:  function(AuthService) {
              // $waitForAuth returns a promise so the resolve waits for it to complete
              return AuthService.auth().$waitForSignIn();
            }
          }
  
        });
    }
  }());
  