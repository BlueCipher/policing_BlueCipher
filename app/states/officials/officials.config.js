(function (){
    'use strict';

    angular
        .module('policingApp.states.officials',[])
        .config(routes);

    function routes($stateProvider){
        $stateProvider
            .state('officials',{
                url:'/officials',
                controller:'OfficialsController',
                controllerAs:'officials',
                templateUrl:'states/officials/officials.html'
            }
        )
            
    }
}
()
);