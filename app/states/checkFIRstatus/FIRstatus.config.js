(function (){
    'use strict';

    angular
        .module('policingApp.states.FIRstatus',[])
        .config(routes);

    function routes($stateProvider){
        $stateProvider
            .state('FIRstatus',{
                url:'/FIRstatus',
                controller:'FIRstatusController',
                controllerAs:'FIRstatus',
                templateUrl:'states/checkFIRstatus/FIRstatus.html'
            }
        )
            
    }
}
()
);