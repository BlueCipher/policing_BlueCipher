(function () {
    'use strict';
  
    angular
      .module('policingApp.states.FIRstatus')
      .controller('FIRstatusController', FIRstatusController);
  
    /* @ngInject */
   function FIRstatusController($state, $firebaseArray, ViewFIRService,FIRstatusService){
     var vm = this;
     vm.allFIRs=[];
     vm.record;
     vm.viewDetails=viewDetails;
     vm.getFIRs = getAllFIRs;
  
     vm.getFIRs();

  function getAllFIRs(){
    vm.allFIRs= $firebaseArray(ViewFIRService.getTheFIRsList());
 
  }
  function getRecord(id){
    vm.record=allFIRs.$getRecord(id);
  }

  function viewDetails(){
    FIRstatusService.setFIRId(vm.FIRSearch.FIRId);
    $state.go('viewFIRstatus');
  }
   }
  }
  ());