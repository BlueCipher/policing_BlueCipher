(function (){
    'use strict';

    angular
        .module('policingApp.states.FIRId',[])
        .config(routes);

    function routes($stateProvider){
        $stateProvider
            .state('FIRId',{
                url:'/FIRId',
                controller:'FIRIdController',
                controllerAs:'FIRId',
                templateUrl:'states/showFIRId/FIRId.html'
            }
        )
            
    }
}
()
);