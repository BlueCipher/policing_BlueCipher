(function () {
  'use strict';

  angular
    .module('policingApp.states.login')
    .controller('LoginController', LoginController);

  /* @ngInject */
 function LoginController(AuthService,$state){
  var vm = this;
  vm.gotoAdminPage = gotoAdminPage;
 
  vm.signIn=function(){
    var result=AuthService.login(vm.email,vm.password);
    result.then(function(authData){
      $state.go('adminPanel');
   }).catch(function(error) {
    window.alert("Incorrect email or password");
   })
  }

   function gotoAdminPage(){
     $state.go('adminPanel');
    }

 }
}
());
