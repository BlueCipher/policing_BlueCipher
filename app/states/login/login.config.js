
(function () {
  'use strict';

  angular
    .module('policingApp.states.login',[])
    .config(routes);

  function routes($stateProvider){
    $stateProvider
      
      .state('login', {
        url: '/login',
        controller:'LoginController',
        controllerAs:'login',
        templateUrl: 'states/login/login.html'

      });
  }
}());
