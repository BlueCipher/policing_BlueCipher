/**
 *
 */

(function () {
    'use strict';
  
    angular
      .module('policingApp.states.newFIR')
      .controller('NewFIRController', NewFIRController);
  
    /* @ngInject */
   function NewFIRController($state, NewFIRService){
     var vm = this;
     vm.submitForm = submitForm;
     vm.name = "Form";

     function submitForm(){
       vm.FIRForm.status= 'recieved';
       NewFIRService.saveFIRForm(vm.FIRForm).then(function (success) {
         
        $state.go('FIRId');
     }).catch(function (error) {
     window.alert('Error');
   });

     }
   }
  }
  ());