/**
 * 
 */

(function () {
    'use strict';
  
    angular
      .module('policingApp.states.newFIR',[])
      .config(routes);
  
    function routes($stateProvider){
      $stateProvider
  
        .state('newFIR', {
          url: '/newFIR',
          controller:'NewFIRController',
          controllerAs:'newFIR',
          templateUrl: 'states/newFIR/newFIR.html'
  
        }) ;
    }
  }());